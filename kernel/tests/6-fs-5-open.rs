#![feature(asm_const)]
#![feature(custom_test_frameworks)]
#![feature(int_roundings)]
#![feature(naked_functions)]
#![feature(slice_group_by)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


extern crate alloc;

use alloc::{format, string::String, vec, vec::Vec};
use core::str;

use ku::memory::size::MiB;

use kernel::{
    fs::{test_scaffolding::BLOCK_SIZE, File, FileSystem, Kind},
    log::info,
    Subsystems,
};


mod gen_main;


gen_main!(Subsystems::MEMORY);


#[test_case]
fn fs() {
    FileSystem::format(FS_DISK, FS_SIZE).unwrap();
    let mut fs = FileSystem::mount(FS_DISK).unwrap();

    test_list(&mut fs, &[]);

    test_basic_operations(&mut fs);

    test_list(
        &mut fs,
        &[
            "/file-1",
            "/file-2",
            "/dir-1",
            "/dir-1/file-4",
            "/dir-1/file-5",
            "/dir-1/dir-2",
            "/dir-1/dir-2/dir-3",
            "/dir-1/dir-2/dir-3/file-3",
        ],
    );

    remove_all(&mut fs);

    test_list(&mut fs, &[]);
}


fn test_basic_operations(fs: &mut FileSystem) {
    let mut root = fs.open("").unwrap();

    root.insert("file-1", Kind::File).unwrap().set_size(5678).unwrap();
    root.insert("file-to-be-erased", Kind::File).unwrap();
    root.insert("file-2", Kind::File).unwrap().write(1234, &[b'*'; 6789]).unwrap();
    let mut dir1 = root.insert("dir-1", Kind::Directory).unwrap();
    dir1.insert("file-4", Kind::File).unwrap();
    dir1.insert("file-5", Kind::File).unwrap();
    let mut dir2 = dir1.insert("dir-2", Kind::Directory).unwrap();
    dir2.insert("dir-3", Kind::Directory)
        .unwrap()
        .insert("file-3", Kind::File)
        .unwrap();

    assert!(root.insert("file-1", Kind::File).is_err());

    fs.open("file-to-be-erased").unwrap().remove().unwrap();

    let mut buffer = [b'-'; 1024];

    assert!(fs.open("file-1").is_ok());
    assert!(fs.open("file-1/").is_err());
    assert!(fs.open("/file-2").is_ok());
    assert!(fs.open("/file-1").unwrap().read(5675, &mut buffer[..16]) == Ok(3));
    assert!(buffer[..3] == [0; 3]);
    assert!(fs.open("/file-1").unwrap().read(5679, &mut buffer[..16]).is_err());
    assert!(fs.open("/file-1").unwrap().read(5678, &mut buffer[..16]) == Ok(0));
    assert!(fs.open("/file-2").unwrap().read(1233, &mut buffer[..1]) == Ok(1));
    assert!(buffer[0] == 0);
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(buffer[..4] == [0, 0, b'*', b'*']);
    assert!(fs.open("/file-2").unwrap().set_size(1232).is_ok());
    assert!(fs.open("/file-2").unwrap().set_size(9876).is_ok());
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(buffer[..4] == [0; 4]);
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(fs.open("/dir-1").is_ok());
    assert!(fs.open("/dir-1/").is_ok());
    assert!(fs.open("/dir-1/file-4").is_ok());
    assert!(fs.open("/dir-1/file-5").is_ok());
    assert!(fs.open("/dir-1/file-5/").is_err());
    assert!(fs.open("/dir-1/dir-2").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3/").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3/file-3").is_ok());
    assert!(fs.open("file-to-be-erased").is_err());
    assert!(fs.open("no-such-file").is_err());
    assert!(fs.open("no-such-dir/file").is_err());
}


fn test_list(fs: &mut FileSystem, expected: &[&str]) {
    let mut actual: Vec<_> = vec![];

    let root = fs.open("").unwrap();
    let usage = build_list(fs, root, "", &mut actual);

    assert_eq!(actual, expected);

    if actual.is_empty() {
        assert_eq!(usage, 0);
        assert_eq!(fs.used_space(), 0);
    }

    assert!(usage <= fs.used_space() + 5678_usize.next_multiple_of(BLOCK_SIZE));
    assert!(fs.used_space() <= 4 * usage);
}


fn build_list(
    fs: &mut FileSystem,
    mut inode: File,
    directory_path: &str,
    list: &mut Vec<String>,
) -> usize {
    let mut usage = 0;

    for entry in inode.list().unwrap() {
        let name = entry.name();
        let path = format!("{directory_path}/{name}");
        info!(path = %&path, %entry);
        list.push(path.clone());
        usage += entry.size().next_multiple_of(BLOCK_SIZE);
        if entry.kind() == Kind::Directory {
            let directory = fs.open(&path).unwrap();
            build_list(fs, directory, &path, list);
        }
    }

    usage
}


fn remove_all(fs: &mut FileSystem) {
    let mut root = fs.open("").unwrap();
    remove_recursive(fs, &mut root, "");
    root.set_size(0).unwrap();
}


fn remove_recursive(fs: &mut FileSystem, inode: &mut File, directory_path: &str) {
    for entry in inode.list().unwrap() {
        let name = entry.name();
        let path = format!("{directory_path}/{name}");
        info!(path = %&path, %entry, "removing");
        let mut file = fs.open(&path).unwrap();
        if entry.kind() == Kind::Directory {
            remove_recursive(fs, &mut file, &path);
        }
        file.remove().unwrap();
    }
}


const FS_DISK: usize = 1;
const FS_SIZE: usize = 32 * MiB;

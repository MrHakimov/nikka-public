#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


use core::cmp;

use chrono::{DateTime, Duration, Utc};
use num_integer;
use x86_64::instructions;

use ku::{
    self,
    time::{
        self,
        pit8254::TICKS_PER_SECOND,
        test_scaffolding::{datetime_with_resolution, forge_tsc, new_correlation_interval},
    },
};

use kernel::{
    interrupts::{INTERRUPT_STATS, RTC, TIMER},
    log::debug,
    Subsystems,
};


mod gen_main;

gen_main!(Subsystems::empty());


#[test_case]
fn points_beyond_interval() {
    wait_for_two_correlation_points();

    let a_few_seconds_in_tsc = 10_000_000_000;

    let before_boot = time::datetime(forge_tsc(-a_few_seconds_in_tsc));
    let boot = time::datetime(forge_tsc(0));
    let now = time::now();
    let future = time::datetime(forge_tsc(time::tsc() + a_few_seconds_in_tsc));

    debug!(%before_boot);
    debug!(%boot);
    debug!(%now);
    debug!(%future);

    check_difference(before_boot, boot);
    check_difference(boot, now);
    check_difference(now, future);

    fn check_difference(left: DateTime<Utc>, right: DateTime<Utc>) {
        let difference = right - left;

        debug!(%left, %right, %difference);

        assert!(left < right);
        assert!(difference > Duration::milliseconds(100));
        assert!(difference < Duration::seconds(30));
    }
}


#[test_case]
fn stress() {
    wait_for_two_correlation_points();

    stress::<1, 1_000_000>(2_000, 4_000, 3, 7);
    stress::<1, 1_000_000>(2_000, 100, 1, 1);

    fn stress<const TICKS_PER_SECOND: i64, const PARTS_PER_SECOND: i64>(
        tsc_per_tick: i64,
        tsc_end: i64,
        base_tsc_step: usize,
        tsc_step: usize,
    ) {
        let max_delta = Duration::seconds(30);
        let min_delta = Duration::microseconds(100);

        for base_tsc in (1..tsc_end).step_by(base_tsc_step) {
            let correlation_interval =
                new_correlation_interval::<TICKS_PER_SECOND>(base_tsc, base_tsc + tsc_per_tick);

            let point_0 = datetime_with_resolution::<PARTS_PER_SECOND, TICKS_PER_SECOND>(
                &correlation_interval,
                forge_tsc(0),
            );

            for tsc in (-tsc_end..tsc_end).step_by(tsc_step) {
                if tsc == 0 {
                    continue;
                }

                let point_tsc = datetime_with_resolution::<PARTS_PER_SECOND, TICKS_PER_SECOND>(
                    &correlation_interval,
                    forge_tsc(tsc),
                );

                let delta = if tsc < 0 {
                    point_0 - point_tsc
                } else {
                    point_tsc - point_0
                };

                if delta < min_delta || max_delta < delta {
                    debug!(%tsc, %point_tsc, %point_0, %delta, %min_delta, %max_delta, "delta between tsc = point_tsc and Tsc(0) = point_0 is out of bounds");
                    assert!(false);
                }
            }
        }
    }
}


#[test_case]
fn high_resolution_date() {
    wait_for_two_correlation_points();

    const TIMER_TICKS: i64 = 102;
    const NANOSECONDS_PER_TIMER_TICK: i64 = 1_000_000_000 / TICKS_PER_SECOND as i64;
    let mut prev = DateTime::default();
    let timer_ticks = INTERRUPT_STATS[TIMER].count();
    let tick_duration = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK);
    let max_delta = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 6 / 5);
    let min_delta = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 4 / 5);

    let mut samples = 0;
    let mut max = Duration::min_value();
    let mut min = Duration::max_value();
    let mut sum = Duration::zero();
    let mut sum2 = 0;

    debug!(timer_ticks = TIMER_TICKS, "measuring");

    for ticks in 1..TIMER_TICKS {
        while INTERRUPT_STATS[TIMER].count() < timer_ticks + ticks as usize {
            instructions::hlt();
        }

        let now = time::now();

        if ticks > 1 {
            let delta = now - prev;

            if delta < min_delta || max_delta < delta {
                debug!(%ticks, %delta, %prev, %now);
            }

            samples += 1;
            max = cmp::max(max, delta);
            min = cmp::min(min, delta);
            sum = sum + delta;
            let error = (delta - tick_duration).num_nanoseconds().unwrap();
            sum2 += error * error;
        }

        prev = now;
    }

    let max_mean = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 101 / 100);
    let min_mean = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 99 / 100);
    let max_deviation = Duration::milliseconds(5);
    let min_deviation = Duration::nanoseconds(100);

    let mean = sum / samples;
    let deviation = Duration::nanoseconds(num_integer::sqrt(sum2 / i64::from(samples)));

    debug!(samples, %min, %max, %mean, %deviation, "measured");
    debug!(%min_delta, %max_delta, %min_mean, %max_mean, %min_deviation, %max_deviation, "restrictions");

    assert!(
        min_delta <= min && max <= max_delta,
        "one of the measured durations between PIT ticks is not accurate",
    );
    assert!(
        min_mean <= mean && mean <= max_mean,
        "measured mean PIT frequency is not accurate",
    );
    assert!(
        min_deviation <= deviation && deviation <= max_deviation,
        "measured deviation of the PIT frequency is too big",
    );
}


fn wait_for_two_correlation_points() {
    debug!("waiting for the RTC to tick twice");

    let rtc_ticks = INTERRUPT_STATS[RTC].count();
    while INTERRUPT_STATS[RTC].count() < rtc_ticks + 2 {
        instructions::hlt();
    }
}

#![feature(custom_test_frameworks)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


use core::mem;

use ku::memory::size::MiB;

use kernel::{
    log::debug,
    memory::{Block, BASE_ADDRESS_SPACE, KERNEL_RW},
    Subsystems,
};


mod gen_main;

gen_main!(Subsystems::PHYS_MEMORY | Subsystems::VIRT_MEMORY);


#[test_case]
fn map_slice() {
    let len = 16 * MiB / mem::size_of::<usize>();
    let slice = BASE_ADDRESS_SPACE.lock().map_slice(len, KERNEL_RW, usize::default).unwrap();

    debug!(slice = ?Block::from_slice(slice));

    for (i, element) in slice.iter_mut().enumerate() {
        *element = i;
    }

    for (i, element) in slice.iter().enumerate() {
        assert_eq!(*element, i);
    }
}

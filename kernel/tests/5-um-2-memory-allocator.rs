#![feature(custom_test_frameworks)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


use ku::{
    error::Error::{InvalidArgument, Overflow, PermissionDenied, WrongAlignment},
    memory::{
        mmu::{KERNEL_READ, KERNEL_RW, USER_READ, USER_RW},
        Block,
        Page,
    },
    process::{Pid, SyscallResult},
    sync::spinlock::Spinlock,
};

use kernel::{
    interrupts::{INTERRUPT_STATS, PAGE_FAULT},
    memory::test_scaffolding::{forbid_frame_leaks, switch_to},
    process::{
        test_scaffolding::{copy_mapping, map, set_pid, unmap},
        Process,
        Scheduler,
    },
    Subsystems,
};


mod gen_main;
mod process_helpers;


gen_main!(Subsystems::MEMORY | Subsystems::SYSCALL | Subsystems::SMP | Subsystems::PROCESS);


const MEMORY_ALLOCATOR_ELF: &[u8] = page_aligned!("../../target/kernel/user/memory_allocator");


#[test_case]
fn map_syscall_group() {
    let _guard = forbid_frame_leaks();

    let process = Spinlock::new(process_helpers::make(MEMORY_ALLOCATOR_ELF));
    set_pid(&mut process.lock(), Pid::new(0));
    let pid = process.lock().pid().into_usize();
    switch_to(process.lock().address_space());

    let block = Block::from_slice("some kernel memory".as_bytes()).enclosing();
    map_over_kernel(&process, block);

    let block = process.lock().address_space().allocate(2 * Page::SIZE).unwrap();
    let mut head = block;
    let tail = head.tail(1).unwrap();
    unsafe {
        process.lock().address_space().map_block(tail, KERNEL_RW).unwrap();
    }
    map_over_kernel(&process, block);
    unsafe {
        process.lock().address_space().unmap_block(tail).unwrap();
    }

    let block = process.lock().address_space().allocate(block.size()).unwrap();
    let address = block.start_address().into_usize();
    let size = block.size();
    let new_block = process.lock().address_space().allocate(block.size()).unwrap();
    let new_address = new_block.start_address().into_usize();

    for flags in [KERNEL_READ, KERNEL_RW] {
        let flags = flags.bits();
        assert_eq!(
            map(process.lock(), pid, address, size, flags),
            Err(PermissionDenied),
        );
    }

    for flags in [USER_READ, USER_RW] {
        let flags = flags.bits();
        assert_eq!(
            map(process.lock(), pid, address, size, flags),
            Ok(SyscallResult(address)),
        );
        for flags in [KERNEL_READ, KERNEL_RW] {
            let flags = flags.bits();
            assert_eq!(
                copy_mapping(process.lock(), pid, address, new_address, size, flags),
                Err(PermissionDenied),
            );
        }
        assert!(copy_mapping(process.lock(), pid, address, new_address, size, flags).is_ok());
    }

    let flags = USER_RW.bits();
    assert!(unmap(process.lock(), pid, address, size).is_ok());
    assert!(unmap(process.lock(), pid, new_address, size).is_ok());
    assert_eq!(
        copy_mapping(process.lock(), pid, address, new_address, size, flags),
        Err(PermissionDenied),
    );
    assert_eq!(
        unmap(process.lock(), pid, address, size),
        Err(PermissionDenied),
    );

    assert!(map(process.lock(), pid, 0, size, flags).is_ok());
    assert_eq!(
        map(process.lock(), pid, 1, size, flags),
        Err(WrongAlignment),
    );
    for address in [0, address] {
        assert_eq!(
            map(process.lock(), pid, address, 0, flags),
            Err(InvalidArgument),
        );
        for size in [1, size + 1] {
            assert_eq!(
                map(process.lock(), pid, address, size, flags),
                Err(WrongAlignment),
            );
        }
    }

    for (address, size) in [
        (0x1_0000, 0xFFFF_FFFF_0000_0000),
        (0xFFFF_FFFF_FFFF_0000, 0x10_0000),
    ] {
        let result = map(process.lock(), pid, address, size, flags);
        assert!(
            result == Err(InvalidArgument) || result == Err(Overflow),
            "expected Err(InvalidArgument) or Err(Overflow), got {:?}",
            result,
        );
    }
}


fn map_over_kernel(process: &Spinlock<Process>, block: Block<Page>) {
    let pid = process.lock().pid().into_usize();

    let address = block.start_address().into_usize();
    let size = block.size();
    let new_block = process.lock().address_space().allocate(block.size()).unwrap();
    let new_address = new_block.start_address().into_usize();

    assert_eq!(
        unmap(process.lock(), pid, address, size),
        Err(PermissionDenied),
    );

    for flags in [KERNEL_READ, KERNEL_RW, USER_READ, USER_RW] {
        let flags = flags.bits();

        assert_eq!(
            map(process.lock(), pid, address, size, flags),
            Err(PermissionDenied),
        );
        assert_eq!(
            copy_mapping(process.lock(), pid, address, new_address, size, flags),
            Err(PermissionDenied),
        );
    }
}


#[test_case]
fn user_space_memory_allocator() {
    let _guard = forbid_frame_leaks();

    Scheduler::enqueue(process_helpers::allocate(MEMORY_ALLOCATOR_ELF).pid());

    while Scheduler::run_one() {}

    assert!(
        INTERRUPT_STATS[PAGE_FAULT].count() == 0,
        "the user mode code has detected an error in the memory allocator implementation",
    );
}

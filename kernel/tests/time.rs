#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


use core::{
    cmp,
    sync::atomic::{AtomicI64, AtomicUsize, Ordering},
};

use chrono::{DateTime, Duration};
use num_integer;
use x86_64::instructions;

use ku::{
    memory::{Block, Virt},
    process::registers::RFlags,
    time::{self, pit8254::TICKS_PER_SECOND, test_scaffolding::AtomicCorrelationPoint},
};

use kernel::{
    interrupts::{self, InterruptContext, INTERRUPT_STATS, RTC, TIMER},
    log::debug,
    time::test_scaffolding::{parse_hour, RegisterB},
    Subsystems,
};


mod gen_main;

gen_main!(Subsystems::empty());


#[test_case]
fn different_rtc_formats() {
    for hour_24 in 0..24 {
        let pm = hour_24 / 12;
        let hour_12 = ((hour_24 + 11) % 12) + 1;

        debug!(hour_24, hour_12, pm);

        assert_eq!(
            hour_24,
            parse_hour(
                hour_24,
                RegisterB::USE_BINARY_FORMAT | RegisterB::USE_24_HOUR_FORMAT,
            ),
        );
        assert_eq!(
            hour_24,
            parse_hour(pack(hour_12, pm), RegisterB::USE_BINARY_FORMAT)
        );
        assert_eq!(
            hour_24,
            parse_hour(bcd(hour_24), RegisterB::USE_24_HOUR_FORMAT)
        );
        assert_eq!(
            hour_24,
            parse_hour(pack(bcd(hour_12), pm), RegisterB::empty())
        );
    }

    fn bcd(x: u8) -> u8 {
        (x / 10) * 16 + (x % 10)
    }

    fn pack(hour: u8, pm: u8) -> u8 {
        hour | (pm << 7)
    }
}


#[test_case]
fn rtc_read_inconsistent() {
    let start = time::now();
    let rtc_ticks = INTERRUPT_STATS[RTC].count();
    const TICKS: i64 = 10;

    debug!(%start);
    assert!(
        start > DateTime::parse_from_rfc3339("2022-09-10T11:50:17+00:00").unwrap(),
        "the RTC date does not pass the sanity check"
    );

    for ticks in 1..TICKS {
        let mut rtc_count = INTERRUPT_STATS[RTC].count();

        while rtc_count < rtc_ticks + (ticks as usize) {
            instructions::hlt();

            let new_rtc_count = INTERRUPT_STATS[RTC].count();
            if new_rtc_count != rtc_count {
                debug!(rtc_count, new_rtc_count);
                assert!(new_rtc_count == rtc_count + 1, "unexpected RTC tick");
            }
            rtc_count = new_rtc_count;
        }

        let now = time::now();
        let max_now = start + Duration::seconds(ticks + 1);
        let min_now = start + Duration::seconds(ticks - 1);

        debug!(%now, rtc_count);

        assert!(
            min_now <= now && now <= max_now,
            "the RTC date does not follow its ticks",
        );
    }
}


#[test_case]
fn correlation_point_reader() {
    interrupts::test_scaffolding::set_debug_handler(writer);

    reader();

    static POINT: AtomicCorrelationPoint = AtomicCorrelationPoint::new();

    fn reader() {
        let mut prev = POINT.load();
        let mut different = 0;
        let mut same = 0;
        let rtc_ticks = AtomicUsize::new(0);

        const ITERATIONS: usize = 10_000;

        while same < ITERATIONS || different < ITERATIONS {
            switch_trap_flag();
            let point = POINT.load();
            switch_trap_flag();

            if prev == point {
                same += 1;
            } else {
                different += 1;
            }

            if sample(&rtc_ticks) {
                debug!(same, different, ?point);
            }

            assert!(
                2 * point.count() == point.tsc(),
                "{:?} is inconsistent",
                point
            );

            prev = point;
        }
    }

    extern "x86-interrupt" fn writer(_: InterruptContext) {
        static VALUE: AtomicI64 = AtomicI64::new(0);

        let mut value = VALUE.fetch_add(1, Ordering::Relaxed);
        if value % 37 == 17 {
            value -= 1;
        }

        match (value / 1_000) % 4 {
            0 => POINT.store(time::test_scaffolding::new_point(value + 1, 2 * value + 2)),
            2 => POINT.inc(2 * POINT.load().count() + 2),
            _ => {},
        }
    }
}


#[test_case]
fn correlation_point_writer() {
    interrupts::test_scaffolding::set_debug_handler(reader);

    writer();

    const LAST_ITERATION: i64 = 10_000;

    static ITERATION: AtomicI64 = AtomicI64::new(0);
    static POINT: AtomicCorrelationPoint = AtomicCorrelationPoint::new();

    fn writer() {
        switch_trap_flag();

        for iteration in 0..LAST_ITERATION / 2 {
            let value = iteration - if iteration % 37 == 17 { 1 } else { 0 };
            POINT.store(time::test_scaffolding::new_point(value, 2 * value));
            ITERATION.store(iteration, Ordering::Relaxed);
        }

        for iteration in LAST_ITERATION / 2..=LAST_ITERATION {
            POINT.inc(2 * POINT.load().count() + 2);
            ITERATION.store(iteration, Ordering::Relaxed);
        }

        switch_trap_flag();
    }

    extern "x86-interrupt" fn reader(_: InterruptContext) {
        static FAILURE_COUNT: AtomicI64 = AtomicI64::new(0);
        static SUCCESS_COUNT: AtomicI64 = AtomicI64::new(0);
        static RTC_TICKS: AtomicUsize = AtomicUsize::new(0);

        let iteration = ITERATION.load(Ordering::Relaxed);
        let failure_count = FAILURE_COUNT.load(Ordering::Relaxed);
        let success_count = SUCCESS_COUNT.load(Ordering::Relaxed);

        if let Some(point) = time::test_scaffolding::try_load(&POINT) {
            if sample(&RTC_TICKS) {
                debug!(iteration, failure_count, success_count, ?point);
                ITERATION.fetch_add(1, Ordering::Relaxed);
            }
            assert!(
                2 * point.count() == point.tsc(),
                "{:?} is inconsistent",
                point
            );
            SUCCESS_COUNT.fetch_add(1, Ordering::Relaxed);
        } else {
            FAILURE_COUNT.fetch_add(1, Ordering::Relaxed);
        }

        if iteration == LAST_ITERATION {
            debug!(iteration, failure_count, success_count);
            assert!(
                success_count >= LAST_ITERATION && failure_count >= LAST_ITERATION,
                "the test is too weak"
            );
            ITERATION.fetch_add(1, Ordering::Relaxed);
        }
    }
}


#[test_case]
fn high_resolution_date() {
    wait_for_two_correlation_points();

    const TIMER_TICKS: i64 = 102;
    const NANOSECONDS_PER_TIMER_TICK: i64 = 1_000_000_000 / TICKS_PER_SECOND as i64;
    let mut prev = DateTime::default();
    let timer_ticks = INTERRUPT_STATS[TIMER].count();
    let tick_duration = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK);
    let max_delta = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 6 / 5);
    let min_delta = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 4 / 5);

    let mut samples = 0;
    let mut max = Duration::min_value();
    let mut min = Duration::max_value();
    let mut sum = Duration::zero();
    let mut sum2 = 0;

    debug!(timer_ticks = TIMER_TICKS, "measuring");

    for ticks in 1..TIMER_TICKS {
        while INTERRUPT_STATS[TIMER].count() < timer_ticks + ticks as usize {
            instructions::hlt();
        }

        let now = time::now();

        if ticks > 1 {
            let delta = now - prev;

            if delta < min_delta || max_delta < delta {
                debug!(%ticks, %delta, %prev, %now);
            }

            samples += 1;
            max = cmp::max(max, delta);
            min = cmp::min(min, delta);
            sum = sum + delta;
            let error = (delta - tick_duration).num_nanoseconds().unwrap();
            sum2 += error * error;
        }

        prev = now;
    }

    let max_mean = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 101 / 100);
    let min_mean = Duration::nanoseconds(NANOSECONDS_PER_TIMER_TICK * 99 / 100);
    let max_deviation = Duration::milliseconds(5);
    let min_deviation = Duration::nanoseconds(100);

    let mean = sum / samples;
    let deviation = Duration::nanoseconds(num_integer::sqrt(sum2 / i64::from(samples)));

    debug!(samples, %min, %max, %mean, %deviation, "measured");
    debug!(%min_delta, %max_delta, %min_mean, %max_mean, %min_deviation, %max_deviation, "restrictions");

    assert!(
        min_delta <= min && max <= max_delta,
        "one of the measured durations between PIT ticks is not accurate"
    );
    assert!(
        min_mean <= mean && mean <= max_mean,
        "measured mean PIT frequency is not accurate"
    );
    assert!(
        min_deviation <= deviation && deviation <= max_deviation,
        "measured deviation of the PIT frequency is too big"
    );
}


#[allow(dead_code)]
// #[test_case] // Uncomment it if you wish.
fn down_the_rabbit_hole() {
    interrupts::test_scaffolding::set_debug_handler(collect_statistics);

    wait_for_two_correlation_points();

    let mut timer = ku::timer();
    debug!(rflags = %RFlags::read(), "how many instructions does it take to log something?");
    let time_to_log_a_message = timer.lap();

    switch_trap_flag();
    debug!(rflags = %RFlags::read(), "how many instructions does it take to log something?");
    switch_trap_flag();
    let time_to_log_a_message_in_the_stepping_mode = timer.elapsed();

    let max_rsp = MAX_RSP.load(Ordering::Relaxed);
    let min_rsp = cmp::min(max_rsp, MIN_RSP.load(Ordering::Relaxed));
    let rip = RIP.load(Ordering::Relaxed);

    debug!(
        instruction_count = INSTRUCTION_COUNT.load(Ordering::Relaxed),
        used_stack_space = %Block::<Virt>::from_index(min_rsp, max_rsp).unwrap(),
        last_traced_insruction_address = %Virt::new(rip).unwrap(),
    );

    let stepping_slowdown_ratio =
        time_to_log_a_message_in_the_stepping_mode.into_f64() / time_to_log_a_message.into_f64();
    debug!(%time_to_log_a_message, %time_to_log_a_message_in_the_stepping_mode, stepping_slowdown_ratio);

    static INSTRUCTION_COUNT: AtomicUsize = AtomicUsize::new(0);
    static MAX_RSP: AtomicUsize = AtomicUsize::new(usize::MIN);
    static MIN_RSP: AtomicUsize = AtomicUsize::new(usize::MAX);
    static RIP: AtomicUsize = AtomicUsize::new(0);

    extern "x86-interrupt" fn collect_statistics(context: InterruptContext) {
        let context = context.get().mini_context();
        let rip = context.rip().into_usize();
        let rsp = context.rsp().into_usize();

        let max_rsp = cmp::max(rsp, MAX_RSP.load(Ordering::Relaxed));
        let min_rsp = cmp::min(rsp, MIN_RSP.load(Ordering::Relaxed));

        INSTRUCTION_COUNT.fetch_add(1, Ordering::Relaxed);
        MAX_RSP.store(max_rsp, Ordering::Relaxed);
        MIN_RSP.store(min_rsp, Ordering::Relaxed);
        RIP.store(rip, Ordering::Relaxed);
    }
}


fn sample(ticks: &AtomicUsize) -> bool {
    let prev_rtc_ticks = ticks.load(Ordering::Relaxed);
    let rtc_ticks = INTERRUPT_STATS[RTC].count();
    if prev_rtc_ticks != rtc_ticks {
        ticks.store(rtc_ticks, Ordering::Relaxed);
        true
    } else {
        false
    }
}


fn switch_trap_flag() {
    let new_flags = RFlags::read() ^ RFlags::TRAP_FLAG;
    unsafe {
        new_flags.write();
    }
}


fn wait_for_two_correlation_points() {
    debug!("waiting for the RTC to tick twice");

    let rtc_ticks = INTERRUPT_STATS[RTC].count();
    while INTERRUPT_STATS[RTC].count() < rtc_ticks + 2 {
        instructions::hlt();
    }
}

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

//! Общая для пространств ядра и пользователя библиотека.
//! ku --- **k**ernel && **u**ser.

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![deny(warnings)]
#![feature(allocator_api)]
#![feature(cell_update)]
#![feature(const_caller_location)]
#![feature(let_chains)]
#![feature(ptr_as_uninit)]
#![feature(slice_ptr_get)]
#![feature(step_trait)]
#![feature(strict_provenance)]
#![no_std]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(missing_docs)]


extern crate rlibc;

/// Аллокаторы памяти общего назначения.
pub mod allocator;

/// Поддержка печати бектрейсов.
pub mod backtrace;

/// Перечисление для возможных ошибок [`Error`] и соответствующий [`Result`].
pub mod error;

/// Ядро предоставляет пользовательским процессам часть информации о системе,
/// сохраняя её в памяти, доступной пользователю на чтение.
/// Эта информация собирается в виде структуры общей информации о системе [`SystemInfo`]
/// и в виде структуры с информацией о текущем процессе [`ProcessInfo`].
pub mod info;

/// Поддержка логирования макросами библиотеки [`tracing`].
/// Сериализует сообщения в [`ring_buffer`] и
/// передаёт их для логирования в ядро через [`ProcessInfo::log()`].
/// Сериализация осуществляется с помощью [`serde`] в формате [`postcard`].
pub mod log;

/// Здесь собраны базовые примитивы для работы с памятью,
/// которые нужны и в ядре, и в пространстве пользователя.
pub mod memory;

/// Здесь собраны функции и структуры для работы с процессами,
/// которые нужны и в ядре, и в пространстве пользователя.
pub mod process;

/// [Непрерывный циклический буфер](https://fgiesen.wordpress.com/2012/07/21/the-magic-ring-buffer/).
pub mod ring_buffer;

/// Примитивы синхронизации [`Spinlock`] и [`SequenceLock`].
pub mod sync;

/// Здесь собраны базовые примитивы для работы со временем,
/// которые нужны и в ядре, и в пространстве пользователя.
pub mod time;


pub use error::{Error, Result};
pub use info::{
    process_info,
    set_process_info,
    set_system_info,
    system_info,
    ProcessInfo,
    SystemInfo,
};
pub use ring_buffer::{ReadBuffer, RingBufferReadTx, RingBufferWriteTx, WriteBuffer};
pub use sync::{sequence_lock::SequenceLock, spinlock::Spinlock};
pub use time::{delay, now, now_ms, timer, tsc, Hz, Tsc, TscDuration};

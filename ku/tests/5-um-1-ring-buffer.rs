#![deny(warnings)]
#![feature(slice_group_by)]


use std::{
    fmt,
    thread,
    time::{Duration, Instant},
};

use ntest_timeout::timeout;
use rand::{distributions::Alphanumeric, rngs::SmallRng, Rng, SeedableRng};

use ku::{
    error::{Error, Result},
    log::{debug, trace},
    memory::{
        size::{MiB, Size},
        Page,
    },
    ring_buffer::{
        self,
        Error::Overflow,
        ReadBuffer,
        RingBufferWriteTx,
        WriteBuffer,
        MAX_CAPACITY,
    },
};

use mmap_allocator::MmapAllocator;


mod log;
mod mmap_allocator;


const QUALITY: Quality = Quality::Paranoid;
const SEED: u64 = 314159265;
const WINDOW_DURATION: Duration =
    Duration::from_millis(if cfg!(debug_assertions) { 500 } else { 1 });


#[test]
#[timeout(1_000)]
fn check_continuous_mapping() {
    log::init();

    let mut allocator = MmapAllocator::new();

    let (_, write_buffer) = ring_buffer::make_pipe(&mut allocator).unwrap();
    let block = write_buffer.block();

    debug!(ring_buffer_block = %block);
    assert_eq!(block.start_address().into_usize() % Page::SIZE, 0);
    assert_eq!(block.size() % (2 * Page::SIZE), 0);
    assert!(block.size() > 2 * Page::SIZE);

    let mut read_rng = SmallRng::seed_from_u64(SEED);
    let mut write_rng = read_rng.clone();

    let buf = unsafe { block.try_into_mut_slice::<u8>().unwrap() };
    let (read_buf, write_buf) = buf.split_at_mut(block.size() / 2);

    for write_ref in write_buf {
        let value = write_rng.gen();
        let write_ptr = write_ref as *mut u8;
        unsafe {
            write_ptr.write_volatile(value);
        }
    }

    for read_ref in read_buf {
        let value = read_rng.gen();
        let read_ptr = read_ref as *const u8;
        assert_eq!(
            unsafe { read_ptr.read_volatile() },
            value,
            "RingBuffer is not mapped properly to be continuous",
        );
    }

    allocator.unmap();
}


#[test]
#[timeout(60_000)]
fn sequential() {
    log::init();

    let mut allocator = MmapAllocator::new();

    let (mut read_buffer, mut write_buffer) = ring_buffer::make_pipe(&mut allocator).unwrap();

    let mut rng = SmallRng::seed_from_u64(SEED);
    let mut read_ops = Generator::new(QUALITY, SEED);
    let mut write_ops = read_ops.clone();
    let mut write_errors = 0;

    for iteration in 0..10_000 {
        if iteration % 1_000 == 0 {
            debug!(iteration, read_stats = ?read_buffer.read_stats(), write_stats = ?write_buffer.write_stats());
        }

        if rng.gen_ratio(3, 4) {
            write_errors += write_with_retries(
                &mut read_buffer,
                &mut write_buffer,
                &mut write_ops,
                &mut read_ops,
                &mut rng,
            );
        } else {
            read(&mut read_buffer, &mut read_ops, &mut rng);
        }
    }

    allocator.unmap();

    assert!(
        write_errors > 0,
        "the test does not check for RingBuffer overflow",
    );
}


#[test]
#[timeout(300_000)]
fn concurrent() {
    log::init();

    const ITERATIONS: u64 = if cfg!(debug_assertions) { 20 } else { 1_000 };

    for iteration in 0..ITERATIONS {
        let mut allocator = MmapAllocator::new();

        let (read_buffer, write_buffer) = ring_buffer::make_pipe(&mut allocator).unwrap();

        let read_thread = thread::Builder::new()
            .name("read_thread".to_string())
            .spawn(move || read_thread(iteration, SEED + iteration, read_buffer))
            .unwrap();

        let write_thread = thread::Builder::new()
            .name("write_thread".to_string())
            .spawn(move || write_thread(iteration, SEED + iteration, write_buffer))
            .unwrap();

        assert!(read_thread.join().is_ok());
        assert!(write_thread.join().is_ok());

        allocator.unmap();
    }
}


fn read_thread(iteration: u64, seed: u64, mut read_buffer: ReadBuffer) {
    let mut rng = SmallRng::seed_from_u64(seed);
    let mut read_ops = Generator::new(QUALITY, seed);

    let mut read_tx = 0;
    let mut throughtput_checks = 0;
    let mut window_bytes = 0;
    let mut window_begin = Instant::now();

    while let Some(bytes) = read(&mut read_buffer, &mut read_ops, &mut rng) {
        window_bytes += bytes;

        let elapsed = window_begin.elapsed();
        if elapsed >= WINDOW_DURATION {
            let throughput_per_second =
                u128::try_from(window_bytes).unwrap() * 1_000_000 / elapsed.as_micros();
            let throughput_per_second = throughput_per_second.try_into().unwrap();
            let throughput = Size::bytes(throughput_per_second);
            debug!(
                iteration,
                read_tx,
                throughput_per_second = %throughput,
                window_bytes,
                read_stats = ?read_buffer.read_stats(),
            );

            let min_throughtput = if cfg!(debug_assertions) {
                1 * MiB
            } else {
                500 * MiB
            };
            assert!(
                throughput_per_second > min_throughtput,
                "the ring buffer is too slow, throughtput={}/s",
                throughput,
            );

            window_begin = Instant::now();
            window_bytes = 0;
            throughtput_checks += 1;
        }

        if bytes == 0 {
            thread::yield_now();
        }

        read_tx += 1;
    }

    assert!(throughtput_checks >= 2);
}


fn write_thread(iteration: u64, seed: u64, mut write_buffer: WriteBuffer) {
    let mut rng = SmallRng::seed_from_u64(seed);
    let mut write_ops = Generator::new(QUALITY, seed);

    let mut window_begin = Instant::now();

    const WRITE_TRANSACTIONS: u64 = 10_000;

    for write_tx in 0..WRITE_TRANSACTIONS {
        let operation = write_ops.generate_operation();

        if window_begin.elapsed() >= WINDOW_DURATION {
            debug!(iteration, write_tx, write_stats = ?write_buffer.write_stats());
            window_begin = Instant::now();
        }

        while let Err(write_error) = write(&mut write_buffer, &operation, &mut rng) {
            trace!(%operation, ?write_error);
            thread::yield_now();
        }
    }

    write_buffer.close();
}


fn read(
    ring_buffer: &mut ReadBuffer,
    read_ops: &mut Generator,
    rng: &mut SmallRng,
) -> Option<usize> {
    let mut tx = ring_buffer.read_tx()?;
    let mut total_bytes = 0;

    let commit = rng.gen_ratio(1, 2);

    while let Some(data) = unsafe { tx.read() } {
        match read_ops.quality {
            Quality::Debuggable => trace!(
                data = %RunLengthEncoding(data),
                len = data.len(),
                "read_tx",
            ),
            Quality::Paranoid => trace!(?data, len = data.len(), "read_tx"),
        }

        if commit {
            check_read_tx(data, read_ops);
        }

        total_bytes += data.len();
    }

    if commit {
        trace!("read commit");
        tx.commit();
    }

    Some(total_bytes)
}


fn check_read_tx(mut data: &[u8], read_ops: &mut Generator) {
    let mut count = 0;

    while !data.is_empty() {
        let expected = read_ops.generate_operation();
        trace!(data = %RunLengthEncoding(data), operation = %expected, "read");

        if expected.commit {
            let (got, more_data) = data.split_at(expected.data.len());

            // The main point of the stress test ---
            // check that read yields the same data that was written.
            assert!(*got == *expected.data);

            data = more_data;
            count += 1;
        }
    }

    if count > 1 {
        trace!(count, "multiple write transactions in one read transaction");
    }
}


fn write_with_retries(
    read_buffer: &mut ReadBuffer,
    write_buffer: &mut WriteBuffer,
    write_ops: &mut Generator,
    read_ops: &mut Generator,
    rng: &mut SmallRng,
) -> usize {
    let operation = write_ops.generate_operation();
    let mut write_errors = 0;

    trace!(%operation, "starting a chunked write operation");

    while let Err(write_error) = write(write_buffer, &operation, rng) {
        trace!(%operation, ?write_error);

        write_errors += 1;
        assert!(write_errors < 1_000);

        if let Error::RingBuffer(Overflow {
            capacity,
            len,
            exceeding_object_len,
        }) = write_error
        {
            if capacity > 0 && len + exceeding_object_len <= capacity {
                debug!(%operation, ?write_error, "unexpected capacity overflow");
            }
            assert!(capacity == 0 || len + exceeding_object_len > capacity);
        } else {
            panic!("unexpected error {:?}", write_error);
        }

        read(read_buffer, read_ops, rng).unwrap();
    }

    write_errors
}


fn write(ring_buffer: &mut WriteBuffer, operation: &Operation, rng: &mut SmallRng) -> Result<()> {
    let mut tx = ring_buffer.write_tx().unwrap();

    write_chunked(&mut tx, operation, rng)?;

    if operation.commit {
        tx.commit();
    }

    Ok(())
}


fn write_chunked(
    tx: &mut RingBufferWriteTx,
    operation: &Operation,
    rng: &mut SmallRng,
) -> Result<()> {
    let mut chunk_count = 0;
    let mut data = operation.data;

    loop {
        let old_capacity = tx.capacity();
        let (chunk, more_data) = data.split_at(rng.gen_range(0..=data.len()));

        tx.write(chunk)?;

        assert!(tx.capacity() + chunk.len() == old_capacity);

        data = more_data;
        chunk_count += 1;
        if data.is_empty() {
            break;
        }
    }

    trace!(%operation, chunk_count, "write");

    Ok(())
}


#[derive(Clone)]
struct Generator {
    buffer: [u8; MAX_CAPACITY],
    quality: Quality,
    rng: SmallRng,
}


impl Generator {
    fn new(quality: Quality, seed: u64) -> Self {
        Self {
            buffer: [0; MAX_CAPACITY],
            quality,
            rng: SmallRng::seed_from_u64(seed),
        }
    }


    fn generate_operation(&mut self) -> Operation {
        Operation::new(&mut self.buffer, self.quality, &mut self.rng)
    }
}


struct Operation<'a> {
    commit: bool,
    data: &'a [u8],
    quality: Quality,
}


impl<'a> Operation<'a> {
    fn new(data: &'a mut [u8], quality: Quality, rng: &mut SmallRng) -> Self {
        Self {
            commit: rng.gen_ratio(3, 4),
            data: Self::fill(data, quality, rng),
            quality,
        }
    }


    fn fill<'b>(data: &'b mut [u8], quality: Quality, rng: &mut SmallRng) -> &'b [u8] {
        let len = Self::generate_len(rng);

        match quality {
            Quality::Debuggable => data[..len].fill(rng.sample(Alphanumeric) as u8),
            Quality::Paranoid => rng.fill(&mut data[..len]),
        }

        &data[..len]
    }


    fn generate_len(rng: &mut SmallRng) -> usize {
        const BOUNDARY_SIZE: usize = 16;

        if rng.gen_ratio(3, 4) {
            rng.gen_range(BOUNDARY_SIZE..MAX_CAPACITY - BOUNDARY_SIZE + 1)
        } else if rng.gen_ratio(1, 2) {
            rng.gen_range(0..BOUNDARY_SIZE)
        } else {
            rng.gen_range(MAX_CAPACITY - BOUNDARY_SIZE + 1..MAX_CAPACITY + 1)
        }
    }
}


impl fmt::Display for Operation<'_> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self.quality {
            Quality::Debuggable => write!(
                formatter,
                "{:?} x {}, {}",
                if self.data.is_empty() {
                    ' '
                } else {
                    self.data[0] as char
                },
                self.data.len(),
                if self.commit { "commit" } else { "rollback" },
            ),
            Quality::Paranoid => write!(
                formatter,
                "{:?}, {}",
                self.data,
                if self.commit { "commit" } else { "rollback" },
            ),
        }
    }
}


struct RunLengthEncoding<'a>(&'a [u8]);


impl fmt::Display for RunLengthEncoding<'_> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let mut block_count = 0;
        let mut total_len = 0;

        for block in self.0.group_by(|a, b| a == b) {
            write!(formatter, "{:?} x {}, ", block[0] as char, block.len())?;
            block_count += 1;
            total_len += block.len();
        }

        write!(
            formatter,
            "block_count = {}, total_len = {}",
            block_count, total_len,
        )
    }
}


#[allow(unused)]
#[derive(Clone, Copy, Debug)]
enum Quality {
    Debuggable,
    Paranoid,
}

use core::{
    arch::asm,
    mem,
    ptr,
    sync::atomic::{AtomicPtr, Ordering},
};

use static_assertions::const_assert;

use ku::{
    error::{Error::InvalidArgument, Result},
    info,
    info::ProcessInfo,
    memory::{mmu::PageTableFlags, Block, Page, Virt},
    process::{Pid, ResultCode, State, Syscall, TrapInfo, RSP_OFFSET_IN_TRAP_INFO},
};

// Used in docs.
#[allow(unused)]
use crate::syscall;


/// Системный вызов [`syscall::exit()`].
/// Освобождает слот таблицы процессов и возвращается в контекст ядра,
/// из которого пользовательский процесс был запущен.
pub fn exit(code: usize) -> ! {
    syscall(Syscall::EXIT.bits(), code, 0, 0, 0, 0).unwrap();

    unreachable!();
}


/// Системный вызов [`syscall::log_value()`].
/// Логирует строку `message` и число `value`.
pub fn log_value(message: &str, value: usize) -> Result<()> {
    let block = Block::<Virt>::from_slice(message.as_bytes());

    syscall(
        Syscall::LOG_VALUE.bits(),
        block.start_address().into_usize(),
        block.size(),
        value,
        0,
        0,
    )
    .map(|_| ())
}


/// Системный вызов [`syscall::sched_yield()`].
/// Перепланирует процесс в конец очереди готовых к исполнению процессов и
/// забирает у него CPU.
#[allow(unused_must_use)]
pub fn sched_yield() {
    syscall(Syscall::SCHED_YIELD.bits(), 0, 0, 0, 0, 0);
}


/// Системный вызов [`syscall::exofork()`].
/// Создаёт копию вызывающего процесса и возвращает исходному процессу [`Pid`] копии.
/// Внутри копии возвращает [`Pid::Current`].
/// При этом новый процесс создаётся практически без адресного пространства и не готовый к работе.
/// Поэтому он, в частности, не ставится в очередь планировщика.
/// Текущий контекст исходного процесса записывает в копию, чтобы в копии
/// вернуться туда же, куда происходит возврат из системного вызова для вызывающего процесса.
// Inline is needed for the correctness of exofork().
#[inline(always)]
pub fn exofork() -> Result<Pid> {
    let (child_pid, process_info) = syscall(Syscall::EXOFORK.bits(), 0, 0, 0, 0, 0)?;
    let child_pid = Pid::from_usize(child_pid)?;
    if child_pid == Pid::Current {
        info::set_process_info(unsafe { Virt::new(process_info)?.try_into_mut::<ProcessInfo>()? });
    }
    Ok(child_pid)
}


/// Системный вызов [`syscall::map()`].
/// Отображает в памяти процесса, заданного `dst_pid`, блок страниц `dst_block`
/// с флагами доступа `flags`.
/// Если `dst_block.start_address()` равен нулю,
/// сам выбирает свободный участок адресного пространства размера `dst_block.size()`.
pub fn map(dst_pid: Pid, dst_block: Block<Page>, flags: PageTableFlags) -> Result<Block<Page>> {
    let (address, _) = syscall(
        Syscall::MAP.bits(),
        dst_pid.into_usize(),
        dst_block.start_address().into_usize(),
        dst_block.size(),
        flags.bits(),
        0,
    )?;

    let start = Virt::new(address)?;
    let end = (start + dst_block.size())?;

    Block::new(Page::new(start)?, Page::new(end)?)
}


/// Системный вызов [`syscall::unmap()`].
/// Удаляет из виртуальной памяти целевого процесса `dst_pid` блок страниц `dst_block`.
pub fn unmap(dst_pid: Pid, dst_block: Block<Page>) -> Result<()> {
    syscall(
        Syscall::UNMAP.bits(),
        dst_pid.into_usize(),
        dst_block.start_address().into_usize(),
        dst_block.size(),
        0,
        0,
    )
    .map(|_| ())
}


/// Системный вызов [`syscall::copy_mapping()`].
/// Создаёт копию отображения виртуальной памяти из вызывающего процесса
/// в процесс, заданный `dst_pid`.
/// Исходный диапазон задаёт `src_block`, целевой --- `dst_block`.
/// В целевом процессе диапазон будет отображён с флагами `flags`.
/// Не допускает целевое отображение с более широким набором флагов, чем исходное.
/// После выполнения у процессов появляется область
/// [разделяемой памяти](https://en.wikipedia.org/wiki/Shared_memory).
pub fn copy_mapping(
    dst_pid: Pid,
    src_block: Block<Page>,
    dst_block: Block<Page>,
    flags: PageTableFlags,
) -> Result<()> {
    if src_block.count() == dst_block.count() {
        syscall(
            Syscall::COPY_MAPPING.bits(),
            dst_pid.into_usize(),
            src_block.start_address().into_usize(),
            dst_block.start_address().into_usize(),
            dst_block.size(),
            flags.bits(),
        )
        .map(|_| ())
    } else {
        Err(InvalidArgument)
    }
}


/// Системный вызов [`syscall::set_state()`].
/// Переводит целевой процесс, заданный идентификатором `dst_pid`, в заданное состояние `state`.
/// Ставит его в очередь планировщика в случае [`State::RUNNABLE`].
pub fn set_state(dst_pid: Pid, state: State) -> Result<()> {
    syscall(
        Syscall::SET_STATE.bits(),
        dst_pid.into_usize(),
        state.bits(),
        0,
        0,
        0,
    )
    .map(|_| ())
}


/// Системный вызов [`syscall::set_trap_handler()`].
/// Устанавливает для целевого процесса, заданного идентификатором `dst_pid`,
/// пользовательский обработчик прерывания `trap_handler()` со стеком `trap_stack`.
pub fn set_trap_handler(
    dst_pid: Pid,
    trap_handler: fn(&TrapInfo),
    trap_stack: Block<Page>,
) -> Result<()> {
    TRAP_HANDLER.store(trap_handler as *mut _, Ordering::Relaxed);

    syscall(
        Syscall::SET_TRAP_HANDLER.bits(),
        dst_pid.into_usize(),
        trap_trampoline as *const () as usize,
        trap_stack.start_address().into_usize(),
        trap_stack.size(),
        0,
    )
    .map(|_| ())
}


/// Системный вызовов номер `number` с аргументами `arg0`--`arg4`.
// Inline is needed for the correctness of exofork().
#[inline(always)]
pub fn syscall(
    number: usize,
    arg0: usize,
    arg1: usize,
    arg2: usize,
    arg3: usize,
    arg4: usize,
) -> Result<(usize, usize)> {
    // TODO: your code here.
    unimplemented!();
}


/// Получает управление, если в коде пользователя возникло исключение.
/// Сохраняет контекст исключения и передаёт управление обработчику `trap_handler()`
/// установленному с помощью [`syscall::set_trap_handler()`]
/// через вспомогательную функцию [`trap_handler_invoker()`].
/// После выполнения установленного обработчика `trap_handler()` восстанавливает
/// сохранённый контекст.
#[cold]
#[naked]
extern "C" fn trap_trampoline() -> ! {
    unsafe {
        asm!(
            "
            // TODO: your code here.
            ret
            ",

            // TODO: your code here.

            options(noreturn),
        );
    }
}


/// Передаёт управление обработчику `trap_handler()`
/// установленному с помощью [`syscall::set_trap_handler()`].
#[cold]
#[inline(never)]
#[no_mangle]
extern "C" fn trap_handler_invoker(
    // rdi
    info: &mut TrapInfo,
) {
    let trap_handler = TRAP_HANDLER.load(Ordering::Relaxed);
    if !trap_handler.is_null() {
        unsafe {
            const_assert!(mem::size_of::<*const ()>() == mem::size_of::<fn(&TrapInfo)>());
            let trap_handler = mem::transmute::<*const (), fn(&TrapInfo)>(trap_handler);
            (trap_handler)(info);
        }
    }

    unsafe {
        info.prepare_for_ret().unwrap();
    }
}


/// Адрес обработчика `trap_handler()`, установленный с помощью [`syscall::set_trap_handler()`].
static TRAP_HANDLER: AtomicPtr<()> = AtomicPtr::new(ptr::null_mut());

/// Аллокатор памяти общего назначения в пространстве пользователя, реализованный через
/// системные вызовы [`syscall::map()`], [`syscall::unmap()`] и [`syscall::copy_mapping()`].
mod map;


use core::alloc::Layout;

use ku::{
    allocator::{Dispatcher, Info},
    log::debug,
};

use map::MapAllocator;

// Used in docs.
#[allow(unused)]
use crate::syscall;


/// Статистика глобального аллокатора памяти общего назначения в пространстве пользователя.
pub fn info() -> Info {
    GLOBAL_ALLOCATOR.info()
}


/// Распечатывает детальную статистику аллокатора.
pub fn dump_info() {
    debug!(allocator_info = %GLOBAL_ALLOCATOR);
}


/// Обработчик ошибок глобального аллокатора памяти общего назначения.
#[alloc_error_handler]
#[cold]
#[inline(never)]
fn alloc_error_handler(layout: Layout) -> ! {
    panic!("failed to allocate memory, layout = {:?}", layout)
}


/// Глобальный аллокатор памяти общего назначения в пространстве пользователя, реализованный через
/// системные вызовы [`syscall::map()`], [`syscall::unmap()`] и [`syscall::copy_mapping()`].
#[global_allocator]
static GLOBAL_ALLOCATOR: Dispatcher<MapAllocator> = Dispatcher::new(MapAllocator::new());

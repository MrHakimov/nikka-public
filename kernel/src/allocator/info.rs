use core::{
    cmp,
    hint,
    mem,
    sync::atomic::{AtomicUsize, Ordering},
};


pub struct Info {
    name: &'static str,
    allocations: AtomicUsize,
    deallocations: AtomicUsize,
    allocated_pages: AtomicUsize,
    deallocated_pages: AtomicUsize,
    requested_size: AtomicUsize,
    allocated_size: AtomicUsize,
    info_updates: AtomicUsize,
}


impl Info {
    pub(super) const fn new(name: &'static str) -> Self {
        Self {
            name,
            allocations: AtomicUsize::new(0),
            deallocations: AtomicUsize::new(0),
            allocated_pages: AtomicUsize::new(0),
            deallocated_pages: AtomicUsize::new(0),
            requested_size: AtomicUsize::new(0),
            allocated_size: AtomicUsize::new(0),
            info_updates: AtomicUsize::new(0),
        }
    }


    pub fn name(&self) -> &str {
        self.name
    }


    pub fn allocations(&self) -> usize {
        self.allocations.load(Ordering::Relaxed)
    }


    pub fn deallocations(&self) -> usize {
        self.deallocations.load(Ordering::Relaxed)
    }


    pub fn allocated_pages(&self) -> usize {
        self.allocated_pages.load(Ordering::Relaxed)
    }


    pub fn deallocated_pages(&self) -> usize {
        self.deallocated_pages.load(Ordering::Relaxed)
    }


    pub fn requested_size(&self) -> usize {
        self.requested_size.load(Ordering::Relaxed)
    }


    pub fn allocated_size(&self) -> usize {
        self.allocated_size.load(Ordering::Relaxed)
    }


    pub fn current_allocations(&self) -> usize {
        self.allocations() - self.deallocations()
    }


    pub fn current_pages(&self) -> usize {
        self.allocated_pages() - self.deallocated_pages()
    }


    pub fn fragmentation_loss(&self) -> usize {
        self.allocated_size() - self.requested_size()
    }


    pub fn fragmentation_loss_percentage(&self) -> f64 {
        self.fragmentation_loss() as f64 / cmp::max(self.allocated_size(), 1) as f64 * 100.0
    }


    pub(super) fn allocation(
        &self,
        requested_size: usize,
        allocated_size: usize,
        allocated_pages: usize,
    ) {
        self.info_updates.fetch_add(1, Ordering::Acquire);
        self.allocations.fetch_add(1, Ordering::Relaxed);
        self.requested_size.fetch_add(requested_size, Ordering::Relaxed);
        self.allocated_size.fetch_add(allocated_size, Ordering::Relaxed);
        if allocated_pages > 0 {
            self.allocated_pages.fetch_add(allocated_pages, Ordering::Relaxed);
        }
        self.info_updates.fetch_add(1, Ordering::Release);
    }


    pub(super) fn deallocation(
        &self,
        requested_size: usize,
        allocated_size: usize,
        deallocated_pages: usize,
    ) {
        self.info_updates.fetch_add(1, Ordering::Acquire);
        self.deallocations.fetch_add(1, Ordering::Relaxed);
        self.requested_size.fetch_sub(requested_size, Ordering::Relaxed);
        self.allocated_size.fetch_sub(allocated_size, Ordering::Relaxed);
        if deallocated_pages > 0 {
            self.deallocated_pages.fetch_add(deallocated_pages, Ordering::Relaxed);
        }
        self.info_updates.fetch_add(1, Ordering::Release);
    }


    pub(super) fn load(&self) -> Info {
        let mut deallocations;
        let mut allocations;
        let mut deallocated_pages;
        let mut allocated_pages;
        let mut requested_size;
        let mut allocated_size;

        loop {
            let mut info_updates = self.info_updates.load(Ordering::Acquire);

            while info_updates % 2 == 1 {
                hint::spin_loop();
                info_updates = self.info_updates.load(Ordering::Acquire);
            }

            deallocations = self.deallocations.load(Ordering::Relaxed);
            allocations = self.allocations.load(Ordering::Relaxed);
            deallocated_pages = self.deallocated_pages.load(Ordering::Relaxed);
            allocated_pages = self.allocated_pages.load(Ordering::Relaxed);
            requested_size = self.requested_size.load(Ordering::Relaxed);
            allocated_size = self.allocated_size.load(Ordering::Relaxed);

            // Use the 'read-dont-modify-write' trick to be able to `Release`
            // the read section of the sequence lock.
            // See <https://www.hpl.hp.com/techreports/2012/HPL-2012-68.pdf>.
            let curr_info_updates = self.info_updates.fetch_add(0, Ordering::Release);

            if curr_info_updates == mem::replace(&mut info_updates, curr_info_updates) {
                assert!(deallocations <= allocations);
                assert!(deallocated_pages <= allocated_pages);
                assert!(requested_size <= allocated_size);

                break;
            }
        }

        Info {
            name: self.name,
            allocations: AtomicUsize::new(allocations),
            deallocations: AtomicUsize::new(deallocations),
            allocated_pages: AtomicUsize::new(allocated_pages),
            deallocated_pages: AtomicUsize::new(deallocated_pages),
            requested_size: AtomicUsize::new(requested_size),
            allocated_size: AtomicUsize::new(allocated_size),
            info_updates: AtomicUsize::new(0),
        }
    }
}

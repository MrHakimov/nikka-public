#![feature(custom_test_frameworks)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


use kernel::{
    interrupts::{INTERRUPT_STATS, INVALID_OPCODE, PAGE_FAULT},
    memory::test_scaffolding::forbid_frame_leaks,
    process::{test_scaffolding, Process},
    Subsystems,
};


mod gen_main;
mod process_helpers;


gen_main!(
    Subsystems::MEMORY |
        Subsystems::SYSCALL |
        Subsystems::LOCAL_APIC |
        Subsystems::CPUS |
        Subsystems::PROCESS_TABLE
);


const EXIT_ELF: &[u8] = page_aligned!("../../target/kernel/user/exit");
const LOG_VALUE_ELF: &[u8] = page_aligned!("../../target/kernel/user/log_value");


#[test_case]
fn syscall_exit() {
    let _guard = forbid_frame_leaks();

    let mut process = process_helpers::allocate(EXIT_ELF);

    test_scaffolding::disable_interrupts(&mut process);

    Process::enter_user_mode(process);

    assert!(
        INTERRUPT_STATS[INVALID_OPCODE].count() == 0,
        "probably the `syscall` instruction is not initialized",
    );

    assert!(
        INTERRUPT_STATS[PAGE_FAULT].count() == 0,
        concat!(
            "if the Page Fault was in the kernel mode, ",
            "probably the `syscall` instruction is not initialized or ",
            "the kernel has not switched to its own stack; ",
            "if it was in the user mode, maybe the time functions from the first lab ",
            "use `read-dont-modify-write` construction",
        ),
    );
}


#[test_case]
fn syscall_log_value() {
    let _guard = forbid_frame_leaks();

    let mut process = process_helpers::allocate(LOG_VALUE_ELF);

    test_scaffolding::disable_interrupts(&mut process);

    Process::enter_user_mode(process);

    assert!(
        INTERRUPT_STATS[PAGE_FAULT].count() == 0,
        "the user mode code has detected an error in syscall::log_value() implementation",
    );
}

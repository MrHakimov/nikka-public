#![feature(asm_const)]
#![feature(custom_test_frameworks)]
#![feature(naked_functions)]
#![feature(slice_group_by)]
#![feature(sort_internals)]
#![no_main]
#![no_std]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


extern crate alloc;

use alloc::string::String;
use core::str;

use ku::memory::size::MiB;

use kernel::{
    fs::{File, FileSystem, Kind, MAX_NAME_LEN},
    log::info,
    Subsystems,
};


mod gen_main;


gen_main!(Subsystems::MEMORY);


#[test_case]
fn fs() {
    FileSystem::format(FS_DISK, FS_SIZE).unwrap();
    let mut fs = FileSystem::mount(FS_DISK).unwrap();
    let root = fs.open("").unwrap();

    test_list(&mut fs, root, "");
    test(&mut fs);
}


fn test(fs: &mut FileSystem) {
    test_max_name_len(fs);
    test_basic_operations(fs);

    let root = fs.open("").unwrap();
    test_list(fs, root, "");
}


fn test_max_name_len(fs: &mut FileSystem) {
    let mut root = fs.open("").unwrap();
    let name = str::from_utf8(&[b'x'; MAX_NAME_LEN]).unwrap();

    assert!(root.insert(name, Kind::File).is_ok());
    assert!(root.insert(name, Kind::File).is_err());
    fs.open(name).unwrap().remove();
}


fn test_basic_operations(fs: &mut FileSystem) {
    let mut root = fs.open("").unwrap();

    root.insert("file-1", Kind::File).unwrap().set_size(5678).unwrap();
    root.insert("file-to-be-erased", Kind::File).unwrap();
    root.insert("file-2", Kind::File).unwrap().write(1234, &[b'*'; 6789]).unwrap();
    let mut dir1 = root.insert("dir-1", Kind::Directory).unwrap();
    dir1.insert("file-4", Kind::File).unwrap();
    dir1.insert("file-5", Kind::File).unwrap();
    let mut dir2 = dir1.insert("dir-2", Kind::Directory).unwrap();
    dir2.insert("dir-3", Kind::Directory)
        .unwrap()
        .insert("file-3", Kind::File)
        .unwrap();

    assert!(root.insert("file-1", Kind::File).is_err());

    fs.open("file-to-be-erased").unwrap().remove();

    let mut buffer = [b'-'; 1024];

    assert!(fs.open("file-1").is_ok());
    assert!(fs.open("file-1/").is_err());
    assert!(fs.open("/file-2").is_ok());
    assert!(fs.open("/file-1").unwrap().read(5675, &mut buffer[..16]) == Ok(3));
    assert!(buffer[..3] == [0; 3]);
    assert!(fs.open("/file-1").unwrap().read(5679, &mut buffer[..16]).is_err());
    assert!(fs.open("/file-1").unwrap().read(5678, &mut buffer[..16]) == Ok(0));
    assert!(fs.open("/file-2").unwrap().read(1233, &mut buffer[..1]) == Ok(1));
    assert!(buffer[0] == 0);
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(buffer[..4] == [0, 0, b'*', b'*']);
    assert!(fs.open("/file-2").unwrap().set_size(1232).is_ok());
    assert!(fs.open("/file-2").unwrap().set_size(9876).is_ok());
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(buffer[..4] == [0; 4]);
    assert!(fs.open("/file-2").unwrap().read(1232, &mut buffer[..4]) == Ok(4));
    assert!(fs.open("/dir-1").is_ok());
    assert!(fs.open("/dir-1/").is_ok());
    assert!(fs.open("/dir-1/file-4").is_ok());
    assert!(fs.open("/dir-1/file-5").is_ok());
    assert!(fs.open("/dir-1/file-5/").is_err());
    assert!(fs.open("/dir-1/dir-2").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3/").is_ok());
    assert!(fs.open("/dir-1/dir-2/dir-3/file-3").is_ok());
    assert!(fs.open("file-to-be-erased").is_err());
    assert!(fs.open("no-such-file").is_err());
    assert!(fs.open("no-such-dir/file").is_err());
}


fn test_list(fs: &mut FileSystem, mut inode: File, directory_path: &str) {
    for entry in inode.list().unwrap() {
        let mut path = String::new();
        path.push_str(directory_path);
        path.push('/');
        path.push_str(entry.name());
        info!(path = %&path, %entry);
        if entry.kind() == Kind::Directory {
            let directory = fs.open(&path).unwrap();
            test_list(fs, directory, &path);
        }
    }
}


const FS_DISK: usize = 1;
const FS_SIZE: usize = 32 * MiB;
